/**
 * Created by coskudemirhan
 */
var api = function ($http, $q) {

    var encyrptforapi = function(obj) {
        var keys = [];

        if(window.localStorage.getItem('device') !== undefined && window.localStorage.getItem('device') !== null ){
            obj.token = window.localStorage.getItem('device');
        }

        if(window.localStorage.getItem('auth') !== undefined &&  window.localStorage.getItem('auth') !== null ){
            obj.auth = window.localStorage.getItem('auth');
        }

        for(var p in obj)
            if (obj.hasOwnProperty(p) && p !== 'photo') {
                keys.push(p);
            }

        keys.sort();

        console.log(keys);
        var encr = '';

        for(var k in keys){
            if (obj.hasOwnProperty(keys[k])) {
                encr += obj[keys[k]];
            }
        }

        encr += 'C5131ECD52CEF62E43DC3F51B6537';

        return sha256(encr);
    };


    /*
     *
     * APP INITIALIZE & HELPERS
     *
     */

    var api = {};

    api.url = 'http://eventshere.ward.agency/';
    api.device = window.localStorage.getItem('device');
    api.auth = window.localStorage.getItem('auth');

    /**
     * API POST REQUEST HELPER
     * @param endpoint
     * @param method
     * @param data
     * @param successF
     * @param errF
     * @returns {*}
     */

    api.post = function (endpoint, method, data, successF, errF) {
        console.log('Requested:' + endpoint);
        var deferred = $q.defer();
        var url = api.url + endpoint;
        var postData = data;


        if(api.device !== null && api.device !== undefined){
            postData.token = api.device;
            postData.encr = encyrptforapi(data);
        }



        if(window.localStorage.getItem('user.auth') !== null){
            postData.auth = window.localStorage.getItem('user.auth');
        }

        if(method === 'get' || method === 'Get'|| method === 'GET'){
            method = 'GET';
        }else{
            method = 'POST';
        }

        $http({
            method: method,
            url: url,
            params: postData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(
            function (res) {
                successF(res, deferred);
                console.log(res);
            },
            function (err) {
                errF(err, deferred);
            }
        );

        return deferred.promise;
    };


    api.init = function () {

        var defered = $q.defer();

        if (api.device === null || api.device === undefined) {
            var language = 'tr';
            var lang = navigator.language || navigator.userLanguage;


            if (lang === 'en-US' || lang === 'en-GB'){
                language = 'en';
            }

            console.log('test');
            api.register(language).then(function (token) {

                if (token) {
                    defered.resolve(true);
                } else {
                    defered.resolve(false);
                }
            });

        } else {

            if(api.auth !== null && api.auth !== undefined){
                window.location.href='#/tab/home';
            }

            defered.resolve(true);
        }
        return defered.promise;
    };


    /*
     *
     * REQUESTS
     *
     */

    /**
     * DEVICE REGISTER REQUEST
     * @param lang
     */

    api.register = function (lang) {

        var dfr = $q.defer();

        api.device = window.localStorage.getItem('device');


        var post = api.post(
            'device/register',
            'POST',
            {language: lang, type: 'test', app_version: 'dev'},
            function (res, p) {
                if (res.data.status) {
                    console.log(res.data.data.token);
                    window.localStorage.setItem('device', res.data.data.token);
                    p.resolve(res.data.data.token);
                } else {
                    p.resolve(false);
                }

            },

            function (res, p) {
                p.resolve(false);
            }
        );

        post.then(function (token) {
            dfr.resolve(token);
        });

        return dfr.promise;
    };




    api.userLogin = function (email, password) {

        var dfr = $q.defer();

        api.post(
            'user/login',
            'POST',
            {nickname:email, password:password},
            function (res, p) {
                if (res.data.status) {
                    p.resolve(res.data);
                } else {
                    p.resolve(res.data);
                }
            },
            function (res, p) {
                p.resolve(res.data);
            }
            )
            .then(function (user) {
                dfr.resolve(user);
            });


        return dfr.promise;
    }



    api.userProfile = function (userid) {

        var dfr = $q.defer();

        api.post(
            'user/user-profile',
            'GET',
            {user_id:userid},
            function (res, p) {
                if (res.data.status) {
                    p.resolve(res.data);
                } else {
                    p.resolve(res.data);
                }
            },
            function (res, p) {
                p.resolve(res.data);
            }
        )
            .then(function (user) {
                dfr.resolve(user);
            });


        return dfr.promise;
    }




    return api;
}

angular.module('starter').service('apiService', api);
