var controllerModule = angular.module('starter.controllers', [])
.controller('signupCtrl', function($scope) {})
.controller('signup2Ctrl', function($scope) {})
.controller('categoriesCtrl', function($scope) {})
.controller('homeCtrl', function($scope) {})
.controller('feedCtrl', function($scope) {})
.controller('notificationCtrl', function($scope) {})
.controller('profileCtrl', function($scope) {})
.controller('myprofileCtrl', function($scope) {})
.controller('createeventCtrl', function($scope) {})
.controller('createevent2Ctrl', function($scope) {})
.controller('createevent3Ctrl', function($scope) {})
.controller('usermatchCtrl', function($scope) {})

/** Picture Change and Like System **/
.controller('likeCtrl', ['$scope', function($scope) {
  $scope.like = {};
  $scope.like.votes = 0;
  $scope.doVote = function() {
    if ($scope.like.userVotes == 1) {
      delete $scope.like.userVotes;
      $scope.like.votes--;
    } else {
      $scope.like.userVotes = 1;
      $scope.like.votes++;
    }
  }
}])
.controller('ButtonCtrl',function($scope, $ionicPopup, $timeout) {

  $scope.go = function ( path ) {
    $location.path( path );
  };

})
.controller('addUserCtrl', ['$scope', function($scope) {
  $scope.addUser = {};
  $scope.addUser.votes = 0;
  $scope.doVote = function() {
    if ($scope.addUser.userVotes == 1) {
      delete $scope.addUser.userVotes;
      $scope.addUser.votes--;
    } else {
      $scope.addUser.userVotes = 1;
      $scope.addUser.votes++;
    }
  }
}])
.controller('addFriendsCtrl', ['$scope', function($scope) {
  $scope.addFriends = {};
  $scope.addFriends.votes = 0;
  $scope.doVote = function() {
    if ($scope.addFriends.userVotes == 1) {
      delete $scope.addFriends.userVotes;
      $scope.addFriends.votes--;
    } else {
      $scope.addFriends.userVotes = 1;
      $scope.addFriends.votes++;
    }
  }
}])



/** MODAL'S **/
.controller('loginCtrl', function($scope, $ionicModal,apiService) {

  apiService.init();

    $scope.login = {
        email: 'okan.yuksel',
        password: '123456'
    };

    $scope.doLogin = function () {
        apiService.userLogin($scope.login.email,$scope.login.password).then(function (data) {
            window.localStorage.setItem('user',JSON.stringify(data.data.user));
            window.localStorage.setItem('auth',data.data.auth);

            window.location.href='#/tab/home';
        });
    };

    console.log($scope.login );

  $ionicModal.fromTemplateUrl('password.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.password = modal;
  });
  $scope.openModal = function() {
    $scope.password.show();
  };
  $scope.closeModal = function() {
    $scope.password.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.password.remove();
  });
  // Execute action on hide modal
  $scope.$on('password.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('password.removed', function() {
    // Execute action
  });
})
.controller('plusCtrl', function($scope, $ionicModal) {

  $ionicModal.fromTemplateUrl('plus.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.plus = modal;
  });
  $scope.openModal = function() {
    $scope.plus.show();
  };
  $scope.closeModal = function() {
    $scope.plus.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.plus.remove();
  });
  // Execute action on hide modal
  $scope.$on('plus.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('plus.removed', function() {
    // Execute action
  });
})
.controller('postCtrl', function($scope, $ionicModal) {
  $ionicModal.fromTemplateUrl('post-share.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.postshare = modal;
  });
  $scope.openModal = function() {
    $scope.postshare.show();
  };
  $scope.closeModal = function() {
    $scope.postshare.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.postshare.remove();
  });
  // Execute action on hide modal
  $scope.$on('postshare.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('postshare.removed', function() {
    // Execute action
  });


  $ionicModal.fromTemplateUrl('location-share.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.locationshare = modal;
  });
  $scope.openModal = function() {
    $scope.locationshare.show();
  };
  $scope.closeModal = function() {
    $scope.locationshare.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.locationshare.remove();
  });
  // Execute action on hide modal
  $scope.$on('locationshare.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('locationshare.removed', function() {
    // Execute action
  });
})
.controller('locationCtrl', function($scope, $ionicModal) {

})

/** ActionSheet **/

.controller('profileCtrl', function($scope, $ionicActionSheet,apiService) {
  if(window.localStorage.getItem('user') === null){
      window.location.href='#/tab/login';
  }
  var user = JSON.parse(window.localStorage.getItem('user'));
  //loading göster
  apiService.userProfile(user.id).then(function (data) {
      //loading kaldır

      var user_profile = data.data.user;
      var followers = data.data.user.followers;
      var joined_events = data.data.joined_events;
      var created_events = data.data.created_events;


      $scope.name_surname = user_profile.name_surname;
      $scope.bio = user_profile.bio;
      $scope.user_pic = user_profile.pic;
      $scope.facebook = user_profile.facebook;
      $scope.instagram = user_profile.instagram;
      $scope.linkedin = user_profile.linkedin;
      $scope.snapchat = user_profile.snapchat;
      $scope.twitter = user_profile.twitter;

      if(joined_events.length === 0){
          $scope.joined_events_count = 'Herhangi bir etkinliğe katılmadınız';
      }else{
          $scope.joined_events_count = joined_events.length + ' Etkinliğe Katıldınız';
      }


      $scope.followers = followers;
      $scope.joined_events = joined_events;
      $scope.created_events = created_events;


  });

  $scope.convertToDate = function (stringDate){
      return new Date(stringDate);
  };


    $scope.profileSettings = function() {

    $ionicActionSheet.show({
      buttons: [
        { text: 'Etkinliğe Davet Et' },
        { text: 'Etkinlik Bildirimlerini Aç' },
        { text: 'Bu Profili Paylaş' },
        { text: 'Engelle' },
      ],
      destructiveText: 'Şikayet Et',
      cancelText: 'İptal',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        return true;
      },
      destructiveButtonClicked: function() {
        console.log('DESTRUCT');
        return true;
      }
    });
  };
})
.controller('myprofileCtrl', function($scope, $ionicActionSheet) {

  $scope.myprofileSettings = function() {

    $ionicActionSheet.show({
      buttons: [
        { text: 'Yardım Merkezi' },
        { text: 'Sorun Bildir' },
        { text: 'Reklamlar' },
        { text: 'Gizlilik İlkesi' },
        { text: 'Koşullar' },
      ],
      destructiveText: 'Çıkış Yap',
      cancelText: 'İptal',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        return true;
      },
      destructiveButtonClicked: function() {
        console.log('DESTRUCT');
        return true;
      }
    });
  };
});
